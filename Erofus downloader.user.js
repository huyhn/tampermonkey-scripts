// ==UserScript==
// @name         Erofus downloader
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        https://www.erofus.com/comics/*/*
// @require http://code.jquery.com/jquery-latest.js
// @require https://cdn.rawgit.com/eligrey/FileSaver.js/5ed507ef8aa53d8ecfea96d96bc7214cd2476fd2/FileSaver.min.js
// @require https://raw.githubusercontent.com/Stuk/jszip/master/dist/jszip.min.js
// @connect      *
// @grant        GM_getValue
// @grant        GM_setValue
// @grant        GM_xmlhttpRequest
// @grant        GM_info
// @grant        GM.getValue
// @grant        GM.setValue
// @grant        GM.xmlHttpRequest
// @grant        GM.info
// ==/UserScript==

'use strict';
if (typeof GM_getValue === 'undefined' && typeof GM !== 'undefined') {
	var loadSetting = GM.getValue.bind(this, 'ehD-setting');
	self.GM_setValue = GM.setValue;
	self.GM_xmlhttpRequest = GM.xmlHttpRequest;
	self.GM_info = GM.info;
}
(function() {
    var fname_re = /(?:.(?!\/))+$/;
    var img_re = /(.*)(-\d*x\d*)(.*$)/;
    var zip;
    var imageList = [];
    var imageData = [];
    var fileName;
    var zeroFill = function( number, width){
        width -= number.toString().length;
        if ( width > 0 ) {
            return new Array( width + (/\./.test( number ) ? 2 : 1) ).join( '0' ) + number;
        }
        return number + ""; // always return a string
    };
    var storeRes = function(res, index) {
        imageData[index] = res;
        for (var i in res) {
            delete res[i];
        }
    };
    var saveToBlob = function(abData) {
        let blob = new Blob([abData], {type: 'application/vnd.comicbook+zip'});
        $('button#tp-dl').text("Saving to file " + fileName + '.cbz');
        saveAs(blob, fileName + '.cbz');
		setTimeout(function(){
			if ('close' in blob) blob.close();
			blob = null;
		}, 10e3);
        $('button#tp-dl').text("Completed");
    };
    var zipFiles = function(imageData, imageList) {
        for (let j = 0; j < imageData.length; j++) {
            if (imageData[j] != null) {
                zip.file(imageList[j], imageData[j]);
            }
        }
        zip.generateAsync({type: 'arraybuffer'}, function updateCallback(metadata) {
            let msg = "(" + metadata.percent.toFixed(2) + "%)";
            $('button#tp-dl').text('Archiving images to ' + fileName + '.cbz ' + msg);
        }).then(function(abData){
            saveToBlob(abData);
		});
    };
    var dl = function(entries, i, prefixes) {
        let src = entries[i].src.replace(/thumb/, 'medium');
        let fname = (typeof prefixes === 'undefined' ? "" : prefixes[i]) + entries[i].alt;
        console.log("Downloading image " + src + " to " + fname);
        var dlThread = GM_xmlhttpRequest({
            method: 'GET',
            url: src,
            timeout: 2000,
            responseType: 'arraybuffer',
            onload: function(res) {
				var response = res.response;
                imageList[i] = fname;
                console.log("Downloaded " + fname);
				storeRes(response, i);
				for (var index in res) {
					delete res[index];
				}
				response = null;
                if (i + 1 < entries.length) {
                    $('button#tp-dl').text("Downloading (" + i + "/" + entries.length + ") -- "+ fname);
                    dl(entries, i+1, prefixes);
                } else {
                    zipFiles(imageData, imageList);
                }
            },
            ontimeout: function(res) {
                console.log("Cannot download " + fname);
                if (i + 1 < entries.length) {
                    $('button#tp-dl').text("Downloading (" + i + "/" + entries.length + ") -- "+ fname);
                    dl(entries, i+1, prefixes);
                } else {
                    zipFiles(imageData, imageList);
                }
            }
        });
    };
    $(document).ready(function() {
        let $ul = $('ul.text-uppercase');
        fileName = $("title").text().match(/(.*)\s\|/)[1];
        $ul.append('<li><span class="glyphicon glyphicon-chevron-right"></span></li><li><button style="color:black" id="tp-dl">Download</li>');
        $('button#tp-dl').click(function() {
            $(this).attr("disabled", true);
            zip = new JSZip();
            let multiIssue = $('a.a-click div.thumbnail-title').size() > 0;
            if (!multiIssue) {
                let entries = $('a.a-click img');
                if (entries.length > 0) {
                    dl(entries, 0);
                    return
                }
            }
            let total = $('a.a-click').length;
            if (total == 0) return;
            let entries = [];
            let prefixes = [];
            $('a.a-click').each(function(index){
                let issueSrc = document.location.origin + $(this).attr('href');
                let title = $(this).attr('title');
                console.log("Downloading " + title + " with src: " + issueSrc);
                GM_xmlhttpRequest({
                    method: 'GET',
                    url: issueSrc,
                    context, index,
                    timeout: 2000,
                    onload: function(res) {
                        let html = $.parseHTML(res.responseText);
                        let imgs = $(html).find('a.a-click img');
                        for(let i = 0; i < imgs.length; i++) {
                            entries.push(imgs[i]);
                            prefixes.push(title+"/");
                        }
                        if (index == total - 1) {
                            console.log("Last issue " + title);
                            dl(entries, 0, prefixes);
                        }
                    },
                    ontimeout: function(res) { console.log("Cannot download " + issueSrc);}
                });
            });
        });
    });
})();