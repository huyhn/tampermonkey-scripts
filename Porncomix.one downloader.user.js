// ==UserScript==
// @name         Porncomix.one downloader
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        https://www.porncomix.one/gallery/*
// @require http://code.jquery.com/jquery-latest.js
// @require https://cdn.rawgit.com/eligrey/FileSaver.js/5ed507ef8aa53d8ecfea96d96bc7214cd2476fd2/FileSaver.min.js
// @require https://raw.githubusercontent.com/Stuk/jszip/master/dist/jszip.min.js
// @connect      *
// @grant        GM_getValue
// @grant        GM_setValue
// @grant        GM_xmlhttpRequest
// @grant        GM_info
// @grant        GM.getValue
// @grant        GM.setValue
// @grant        GM.xmlHttpRequest
// @grant        GM.info
// ==/UserScript==

'use strict';
if (typeof GM_getValue === 'undefined' && typeof GM !== 'undefined') {
	var loadSetting = GM.getValue.bind(this, 'ehD-setting');
	self.GM_setValue = GM.setValue;
	self.GM_xmlhttpRequest = GM.xmlHttpRequest;
	self.GM_info = GM.info;
}
(function() {
    var fname_re = /(?:.(?!\/))+$/;
    var img_re = /(.*)(-\d*x\d*)(.*$)/;
    var zip;
    var imageList = [];
    var imageData = [];
    var fileName;
    var zeroFill = function( number, width){
        width -= number.toString().length;
        if ( width > 0 ) {
            return new Array( width + (/\./.test( number ) ? 2 : 1) ).join( '0' ) + number;
        }
        return number + ""; // always return a string
    };
    var getfname = function(txt, i) {
        let fname = txt.match(fname_re)[0].substr(1);
        return zeroFill(i + 1, 2) + " - " + fname;
    };
    var storeRes = function(res, index) {
        imageData[index] = res;
        for (var i in res) {
            delete res[i];
        }
    };
    var saveToBlob = function(abData) {
        let blob = new Blob([abData], {type: 'application/vnd.comicbook+zip'});
        $('button#tp-dl').text("Saving to file " + fileName + '.cbz');
        saveAs(blob, fileName + '.cbz');
		setTimeout(function(){
			if ('close' in blob) blob.close();
			blob = null;
		}, 10e3);
        $('button#tp-dl').text("Completed");
    };
    var zipFiles = function(imageData, imageList) {
        for (let j = 0; j < imageData.length; j++) {
            if (imageData[j] != null) {
                zip.file(imageList[j], imageData[j]);
            }
        }
        zip.generateAsync({type: 'arraybuffer'}, function updateCallback(metadata) {
            let msg = "(" + metadata.percent.toFixed(2) + "%)";
            $('button#tp-dl').text('Archiving images to ' + fileName + '.cbz ' + msg);
        }).then(function(abData){
            saveToBlob(abData);
		});
    };
    var dl = function(entries, i, get_src) {
        let src = get_src(entries[i]);
        let fname = getfname(src, i);
        console.log("Making xmlhttpReqest " + src + " to " + fname);
        var dlThread = GM_xmlhttpRequest({
            method: 'GET',
            url: src,
            responseType: 'arraybuffer',
            onload: function(res) {
				var response = res.response;
                imageList[i] = fname;
                console.log("Downloaded " + fname);
				storeRes(response, i);
				for (var index in res) {
					delete res[index];
				}
				response = null;
                if (i + 1 < entries.length) {
                    $('button#tp-dl').text("Downloading (" + i + "/" + entries.length + ") -- "+ fname);
                    dl(entries, i+1, get_src);
                } else {
                    zipFiles(imageData, imageList);
                }
            }
        });
        console.log("Made xmlHttpReqest for " + src);
    };
    $(document).ready(function() {
        let $title = $('h1.entry-title')
        fileName = $title.find("a").text();
        $title.append('<br/><button id="tp-dl">Download</button>');
        $('button#tp-dl').click(function() {
            $(this).attr("disabled", true);
            zip = new JSZip();
            var entries = $('figure.jg-entry a');
            if (entries.length > 0) {
                dl(entries, 0, function(a) {
                    return a.href;
                });
                return
            }
            entries = $('img.ug-thumb-image');
            if (entries.length > 0) {
                dl(entries, 0, function(img) {
                    let results = img.src.match(img_re);
                    return results[1] + results[3];
                });
                return
            }
        });
    });
})();