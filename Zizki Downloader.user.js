// ==UserScript==
// @name         Zizki Downloader
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        https://zizki.com/*/*
// @require http://code.jquery.com/jquery-latest.js
// @require https://cdn.rawgit.com/eligrey/FileSaver.js/5ed507ef8aa53d8ecfea96d96bc7214cd2476fd2/FileSaver.min.js
// @require https://raw.githubusercontent.com/Stuk/jszip/master/dist/jszip.min.js
// @connect      *
// @grant        GM_getValue
// @grant        GM_setValue
// @grant        GM_xmlhttpRequest
// @grant        GM_info
// @grant        GM.getValue
// @grant        GM.setValue
// @grant        GM.xmlHttpRequest
// @grant        GM.info
// ==/UserScript==

'use strict';
if (typeof GM_getValue === 'undefined' && typeof GM !== 'undefined') {
	var loadSetting = GM.getValue.bind(this, 'ehD-setting');
	self.GM_setValue = GM.setValue;
	self.GM_xmlhttpRequest = GM.xmlHttpRequest;
	self.GM_info = GM.info;
}
(function() {
    var fname_re = /(?:.(?!\/))+$/;
    var img_re = /(.*)(-\d*x\d*)(.*$)/;
    var zip;
    var imageList = {};
    var fileName;
    var downloadCount = 0;
    var zeroFill = function( number, width){
        width -= number.toString().length;
        if ( width > 0 ) {
            return new Array( width + (/\./.test( number ) ? 2 : 1) ).join( '0' ) + number;
        }
        return number + ""; // always return a string
    };
    var getExtension = function(txt) {
        return txt.match(/[^\\]*(\.\w+)$/)[1];
    }
    var getfname = function(txt, i, width) {
        if (width === undefined) { width = 2;}
        return zeroFill(i + 1, width) + getExtension(txt);
    };
    var storeRes = function(res, fname) {
        imageList[fname] = res;
        downloadCount++;
        for (var i in res) {
            delete res[i];
        }
    };
    var saveToBlob = function(abData) {
        let blob = new Blob([abData], {type: 'application/vnd.comicbook+zip'});
        $('#tp-dl').text("Saving to file " + fileName + '.cbz');
        saveAs(blob, fileName + '.cbz');
		setTimeout(function(){
			if ('close' in blob) blob.close();
			blob = null;
		}, 10e3);
        $('#tp-dl').text("Completed");
    };
    var zipFiles = function(imageList) {
        $('a#tp-dl').text('Archiving images to ' + fileName + '.cbz');
        for (let path in imageList) {
            zip.file(path, imageList[path]);
        }
        zip.generateAsync({type: 'arraybuffer'}, function updateCallback(metadata) {
            let msg = "(" + metadata.percent.toFixed(2) + "%)";
            $('a#tp-dl').text('Archiving images to ' + fileName + '.cbz ' + msg);
        }).then(function(abData){
            saveToBlob(abData);
		});
    };
    var dl = async function(entries, i) {
        let src = entries[i].src.replace('styles/medium/public/', '').replace(/\?.*$/, '');
        let fname = getfname(src, i, entries.length.toString().length);
        console.log("Downloading image " + src + " to " + fname);
        GM_xmlhttpRequest({
            method: 'GET',
            url: src,
            timeout: 5000,
            responseType: 'arraybuffer',
            onload: function(res) {
                console.log("Downloaded " + fname);
                var response = res.response;
                storeRes(response, fname);
                for (var index in res) {delete res[index] }
                response = null;
                $('#tp-dl').text("Downloaded (" + (downloadCount+1) + "/" + entries.length + ")");
                if (downloadCount === entries.length) {
                    console.log("Downloaded all " + downloadCount + " images");
                    zipFiles(imageList);
                }
            },
            ontimeout: function(res) {
                console.log("Cannot download " + fname);
                if (downloadCount === entries.length) {
                    console.log("Downloaded all " + downloadCount + " images");
                    zipFiles(imageList);
                } else {
                    downloadCount++;
                }
            }
        });
    };
    $(document).ready(function() {
        fileName = "[" + $("span.creator a").text() + "] " + $("h1#page-title").text().trim();
        $('div.pdf_wrapper').append('<div class="downloadpdf"><a id="tp-dl" href="#">Download</a></div>');
        $('a#tp-dl').click(function() {
            $(this).css("pointer-events", "none").css("cursor", "default");
            zip = new JSZip();
            let $issues = $('div.aimage span.atitle');
            let total = $issues.size();
            if (total == 0) {
                let entries = $('div.aimage img');
                let promises = [];
                for (let i = 0; i < entries.length; i++) {
                    dl(entries, i);
                }
                return;
            }
            let entries = [];
            let prefixes = [];
            let width = total.toString().length;
            $issues.each(function(index){
                let $this = $(this).parents('div.aimage').parent();
                let issueSrc = document.location.origin + $this.attr('href');
                let title = $(this).text();
                console.log("Downloading " + title + " with src: " + issueSrc);
                GM_xmlhttpRequest({
                    method: 'GET',
                    url: issueSrc,
                    context, index,
                    timeout: 2000,
                    onload: function(res) {
                        let html = $.parseHTML(res.responseText);
                        let imgs = $(html).find('div.aimage img');
                        for(let i = 0; i < imgs.length; i++) {
                            entries.push(imgs[i]);
                            prefixes.push(title+"/" + zeroFill(i+1,width));
                        }
                        if (index == total - 1) {
                            console.log("Last issue " + title);
                            dl(entries, 0, prefixes);
                        }
                    },
                    ontimeout: function(res) { console.log("Cannot download " + issueSrc);}
                });
            });
        });
    });
})();