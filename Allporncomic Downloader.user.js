// ==UserScript==
// @name         Allporncomic Downloader
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @match        https://allporncomic.com/porncomic/*
// @require http://code.jquery.com/jquery-latest.js
// @require https://cdn.rawgit.com/eligrey/FileSaver.js/5ed507ef8aa53d8ecfea96d96bc7214cd2476fd2/FileSaver.min.js
// @require https://raw.githubusercontent.com/Stuk/jszip/master/dist/jszip.min.js
// @connect      *
// @grant        GM_getValue
// @grant        GM_setValue
// @grant        GM_xmlhttpRequest
// @grant        GM_info
// @grant        GM.getValue
// @grant        GM.setValue
// @grant        GM.xmlHttpRequest
// @grant        GM.info
// ==/UserScript==

'use strict';
if (typeof GM_getValue === 'undefined' && typeof GM !== 'undefined') {
	var loadSetting = GM.getValue.bind(this, 'ehD-setting');
	self.GM_setValue = GM.setValue;
	self.GM_xmlhttpRequest = GM.xmlHttpRequest;
	self.GM_info = GM.info;
}
(function() {
    var downloadCount = {};
    var zeroFill = function( number, width){
        width -= number.toString().length;
        if ( width > 0 ) {
            return new Array( width + (/\./.test( number ) ? 2 : 1) ).join( '0' ) + number;
        }
        return number + ""; // always return a string
    };
    var storeRes = function(res, fname, imageList) {
        imageList[fname] = res;
        for (var i in res) {
            delete res[i];
        }
    };
    var saveToBlob = function(abData, fileName, $dlBtn) {
        let blob = new Blob([abData], {type: 'application/vnd.comicbook+zip'});
        $dlBtn.text("Saving ...");
        console.log("Saving to file " + fileName + '.cbz')
        saveAs(blob, fileName + '.cbz');
		setTimeout(function(){
			if ('close' in blob) blob.close();
			blob = null;
		}, 10e3);
        $dlBtn.text("Completed");
        $dlBtn.trigger('completed')
    };
    var zipFiles = function(imageList, fileName, $dlBtn) {
        let zip = new JSZip();
        $dlBtn.text('Archiving images to ' + fileName + '.cbz');
        for (let path in imageList) {
            zip.file(path, imageList[path]);
        }
        zip.generateAsync({type: 'arraybuffer'}, function updateCallback(metadata) {
            let msg = "(" + metadata.percent.toFixed(2) + "%)";
            $dlBtn.text('Archiving ' + msg);
            console.log('Archiving images to ' + fileName + '.cbz')
        }).then(function(abData){
            saveToBlob(abData, fileName, $dlBtn);
		});
    };
    var dl = async function(img, outfile, total, imageList, $dlBtn, onFinished, retries) {
        let src = img.src.trim();
        let fname = src.match(/.*\/(.*)\?.*$/)[1];
        console.log("Downloading image " + src + " to " + fname);
        GM_xmlhttpRequest({
            method: 'GET',
            url: src,
            timeout: 5000,
            responseType: 'arraybuffer',
            onload: function(res) {
                console.log("Downloaded " + fname);
                var response = res.response;
                storeRes(response, fname, imageList);
                downloadCount[outfile] = downloadCount[outfile]+1;
                for (var index in res) {delete res[index] }
                response = null;
                $dlBtn.text("Downloaded (" + (downloadCount[outfile]) + "/" + total + ")");
                if (downloadCount[outfile] >= total) {
                    console.log("Downloaded all " + total + " images");
                    onFinished(imageList, outfile, $dlBtn);
                }
            },
            ontimeout: function(res) {
                console.log("Cannot download " + fname);
                if (retries >= 5) {
                    console.log("Skipping " + fname);
                    downloadCount[outfile] = downloadCount[outfile]+1;
                    console.log("Cannot download " + fname + " Retrying ..." );
                    if (downloadCount[outfile] >= total) {
                        onFinished(imageList, outfile, $dlBtn);
                    }
                } else {
                    console.log("Retrying " + fname);
                    dl(img, outfile, total, imageList, $dlBtn, onFinished, retries+1);
                }
            }
        });
    };
    var getFilename = function(text, hasOthers) {
        let matches = text.match(/(.*)(\[.*\])$/);
        let artist = (matches === null) ? '[' + $('div.artist-content a:first').text() + '] ' : matches[2];
        let title = (matches === null) ? text : (matches[1].match(/chapter/gi) ? matches[1].replace(/^\d*[\.\d]*\s*\.\s*/g,'') : matches[1]);
        title = hasOthers ? title : title.replace(/\s*-\s*chapter\s\d+\s*/gi, '');
        let outfile = artist + " " + title;
        outfile = outfile.trim().replace(/\s*-\s*$/, '');
        return outfile;
    };
    var getSingleFilename = function(text, hasOthers) {
        let matches = text.match(/(\[.*\])([\s\d\.-]+)(.*)/);
        let chapter = (matches === null || !hasOthers) ? "" : ' ' + matches[2].replace(/[\s\.\-]/g,'');
        let outfile = (matches === null) ? text : matches[1] + " " + matches[3].replace(matches[1], '').trim() + chapter;
        return outfile.trim();
    };
    $(document).ready(function() {
        $('ul.main.version-chap li.wp-manga-chapter').each(function(){
            let $li = $(this);
            $li.find('a:first').addClass("source");
            $li.append('<a href="#" class="btn-chapter-download tp-dl"><i class="icon ion-md-download"></i></a>');
        });
        $('div.nav-links div.nav-next').each(function(){
            $(this).before('<div><a href="#" class="btn tp-dl-single">Download</a></div>');
        });
        $('div#init-links').append('<a href="#" id="dl-all" class="c-btn c-btn_style-1">Download All</a>');
        $('a#dl-all').on('click', function(e) {
            e.preventDefault();
            let $that = $(this);
            $that.css("pointer-events", "none").css("cursor", "default").text("Starting ...");
            let $btns = $('a.tp-dl');
            $btns.each(function(index){
                $(this).on('completed', function() {
                    if (index < $btns.size() - 1) {
                        $($btns[index+1]).trigger('click');
                    } else {
                        $that.text("Completed");
                    }
                });
            });
            $($btns[0]).trigger('click')
        });

        $('a.tp-dl-single').on('click', function(e) {
            e.preventDefault();
            let $dlBtn = $(this);
            $dlBtn.css("pointer-events", "none").css("cursor", "default");
            let outfile = getSingleFilename($('h1#chapter-heading').text().trim(), $('.next_page, .prev_page').size()!=0);
            let imageList = {};
            downloadCount[outfile] = 0;
            $dlBtn.text("Starting ...");
            let imgs = $('img.wp-manga-chapter-img');
            for(let i = 0; i < imgs.length; i++) {
                dl(imgs[i], outfile, imgs.length, imageList, $dlBtn, zipFiles, 0);
            }
        });
        $('a.tp-dl').on('click', function(e) {
            e.preventDefault();
            let $dlBtn = $(this);
            $dlBtn.css("pointer-events", "none").css("cursor", "default").text("Starting ...");
            let $a = $dlBtn.parents('li').find('a.source');
            let issueSrc = $a.attr('href');
            let outfile = getFilename($a.text().trim(), $('a.tp-dl').size()>1);
            let imageList = {};
            downloadCount[outfile] = 0;
            GM_xmlhttpRequest({
                method: 'GET',
                url: issueSrc,
                timeout: 2000,
                onload: function(res) {
                    let html = $.parseHTML(res.responseText);
                    let imgs = $(html).find('img.wp-manga-chapter-img');
                    for(let i = 0; i < imgs.length; i++) {
                        dl(imgs[i], outfile, imgs.length, imageList, $dlBtn, zipFiles, 0);
                    }
                },
                ontimeout: function(res) {
                    console.log("Cannot download " + issueSrc + " Retrying ...");
                    $dlBtn.trigger('click');
                }
            });
        });
    });
})();