// ==UserScript==
// @name         8muses Downloader
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  try to take over the world!
// @author       You
// @downloadURL  https://bitbucket.org/huyhn/tampermonkey-scripts/raw/release/latest/8muses%20Downloader.user.js
// @match        https://www.8muses.com/comics/album/*
// @require http://code.jquery.com/jquery-latest.js
// @require https://cdn.rawgit.com/eligrey/FileSaver.js/5ed507ef8aa53d8ecfea96d96bc7214cd2476fd2/FileSaver.min.js
// @require https://raw.githubusercontent.com/Stuk/jszip/master/dist/jszip.min.js
// @connect      *
// @grant        GM_getValue
// @grant        GM_setValue
// @grant        GM_xmlhttpRequest
// @grant        GM_info
// @grant        GM.getValue
// @grant        GM.setValue
// @grant        GM.xmlHttpRequest
// @grant        GM.info
// ==/UserScript==

'use strict';
if (typeof GM_getValue === 'undefined' && typeof GM !== 'undefined') {
	var loadSetting = GM.getValue.bind(this, 'ehD-setting');
	self.GM_setValue = GM.setValue;
	self.GM_xmlhttpRequest = GM.xmlHttpRequest;
	self.GM_info = GM.info;
}
(function() {
    var fname_re = /(?:.(?!\/))+$/;
    var img_re = /(.*)(-\d*x\d*)(.*$)/;
    var zip;
    var imageList = [];
    var imageData = [];
    var fileName;
    var zeroFill = function( number, width){
        width -= number.toString().length;
        if ( width > 0 ) {
            return new Array( width + (/\./.test( number ) ? 2 : 1) ).join( '0' ) + number;
        }
        return number + ""; // always return a string
    };
    var getfname = function(txt, i) {
        let xtension = txt.match(/[^\\]*\.(\w+)$/)[0];
        return zeroFill(i + 1, 2) + xtension;
    };
    var storeRes = function(res, index) {
        imageData[index] = res;
        for (var i in res) {
            delete res[i];
        }
    };
    var saveToBlob = function(abData) {
        let blob = new Blob([abData], {type: 'application/vnd.comicbook+zip'});
        $('button#tp-dl').text("Saving to file " + fileName + '.cbz');
        saveAs(blob, fileName + '.cbz');
		setTimeout(function(){
			if ('close' in blob) blob.close();
			blob = null;
		}, 10e3);
        $('button#tp-dl').text("Completed");
    };
    var zipFiles = function(imageData, imageList) {
        $('button#tp-dl').text("Archiving images " + fileName + '.cbz');
        for (let j = 0; j < imageData.length; j++) {
            if (imageData[j] != null) {
                zip.file(imageList[j], imageData[j]);
            }
        }
        zip.generateAsync({type: 'arraybuffer'}, function updateCallback(metadata) {
            let msg = "(" + metadata.percent.toFixed(2) + "%)";
            $('button#tp-dl').text('Archiving images to ' + fileName + '.cbz ' + msg);
        }).then(function(abData){
            saveToBlob(abData);
		});
    };
    var dl = function(entries, i, prefixes) {
        let src = entries[i].getAttribute("data-src").replace(/\/th\//, '/fl/');
        let fname = (typeof prefixes === 'undefined' ? getfname(src, i) : prefixes[i] + src.match(/[^\\]*(\.\w+)$/)[1]);
        console.log("Downloading image " + src + " to " + fname);
        var dlThread = GM_xmlhttpRequest({
            method: 'GET',
            url: src,
            timeout: 2000,
            responseType: 'arraybuffer',
            onload: function(res) {
				var response = res.response;
                imageList[i] = fname;
                console.log("Downloaded " + fname);
				storeRes(response, i);
				for (var index in res) {
					delete res[index];
				}
				response = null;
                if (i + 1 < entries.length) {
                    $('button#tp-dl').text("Downloading (" + i + "/" + entries.length + ")");
                    dl(entries, i+1, prefixes);
                } else {
                    zipFiles(imageData, imageList);
                }
            },
            ontimeout: function(res) {
                console.log("Cannot download " + fname);
                if (i + 1 < entries.length) {
                    $('button#tp-dl').text("Downloading (" + i + "/" + entries.length + ")");
                    dl(entries, i+1, prefixes);
                } else {
                    zipFiles(imageData, imageList);
                }
            }
        });
    };
    $(document).ready(function() {
        let fileTitle = $("title").text();
        fileName = fileTitle.match(/(.*)\s\|/) === null ? fileTitle : fileTitle.match(/(.*)\s\|/)[1];
        $('div.top-menu-breadcrumb ol').append('<li><button class="button" style="background: #242730; border: none; color: #6f737f;" id="tp-dl">Download</li>');
        $('button#tp-dl').click(function() {
            $(this).attr("disabled", true);
            zip = new JSZip();
            let $issues = $('div.gallery div.image-title');
            let total = $issues.size();
            if (total == 0) {
                let entries = $('div.gallery img');
                if (entries.length > 0) {
                    dl(entries, 0);
                    return
                }
            }
            let entries = [];
            let prefixes = [];
            $issues.each(function(index){
                let $this = $(this).parents('a.c-tile');
                let issueSrc = document.location.origin + $this.attr('href');
                let title = $this.attr('title');
                console.log("Downloading " + title + " with src: " + issueSrc);
                GM_xmlhttpRequest({
                    method: 'GET',
                    url: issueSrc,
                    context, index,
                    timeout: 2000,
                    onload: function(res) {
                        let html = $.parseHTML(res.responseText);
                        let imgs = $(html).find('div.gallery img');
                        for(let i = 0; i < imgs.length; i++) {
                            entries.push(imgs[i]);
                            prefixes.push(title+"/" + zeroFill(i+1,2));
                        }
                        if (index == total - 1) {
                            console.log("Last issue " + title);
                            dl(entries, 0, prefixes);
                        }
                    },
                    ontimeout: function(res) { console.log("Cannot download " + issueSrc);}
                });
            });
        });
    });
})();